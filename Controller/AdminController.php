<?php

namespace Dziuba\UserBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Dziuba\UserBundle\Entity\User;
//use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends BaseAdminController
{
    public function persistUserEntity(User $user)
    {
        //$password = UserPasswordEncoderInterface::encodePassword($user, $user->getPlainPassword());

        $options = [
            'cost' => 12,
        ];
        $password = password_hash($user->getPlainPassword(), PASSWORD_BCRYPT, $options);
        
        $user->setPassword($password);

        parent::persistEntity($user);
    }
}