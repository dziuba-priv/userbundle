<?php
namespace Dziuba\UserBundle\Controller;

use Dziuba\UserBundle\Form\UserType;
use Dziuba\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Translation\TranslatorInterface;

class SecurityController extends Controller
{
    /**
     * Route(path = "/admin/user/create", name = "user_create")
     * @Route("/register", name="dziuba_user_registration")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('home');
        }

        return $this->render(
            '@DziubaUser/security/register.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/login", name="dziuba_user_login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('@DziubaUser/security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /** 
     * @Route("/reset", name="dziuba_user_password_reset_email")
     */
    public function passwordResetEmail(Request $request, \Swift_Mailer $mailer, TranslatorInterface $translator){

        $form = $this->createFormBuilder()
            ->add('email', EmailType::class)
            ->add('save', SubmitType::class, array('label' => 'Send'))
            ->getForm();
            
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $email = $form->getData()['email'];
            $entityManager = $this->getDoctrine()->getManager();
            $repository = $entityManager->getRepository(User::class);

            $user = $repository->findOneBy(['email' => $email]);

            if($user->getId()){
                $options = [
                    'cost' => 12,
                ];
                $date = new \DateTime();
                $hash = base64_encode(password_hash($email ."|". $date->format("Y.m.dH:i:s"), PASSWORD_BCRYPT, $options));

                // save token in User
                $user->setPasswordToken($hash);
                $entityManager->flush();
    
                // send email
                $dotenv = new Dotenv();
                $dotenv->load($this->get('kernel')->getRootDir() . '/../.env');
                $emailFrom = getenv('MAIL_SEND_FROM');


                $message = (new \Swift_Message($translator->trans('security.password_reset.mail_title')))
                    ->setFrom($emailFrom)
                    ->setTo($email)
                    ->setBody(
                    $this->renderView(
                        // templates/emails/registration.html.twig
                        '@DziubaUser/emails/password.reset.html.twig',
                        array('token' => $hash)
                    ),
                    'text/html'
                )
                /*
                * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
                ;
    
                $mailer->send($message);
        
                //return $this->redirectToRoute('home');
            }
        }

        return $this->render(
            '@DziubaUser/security/password.reset.email.html.twig',
            array('form' => $form->createView())
        );
    }

    /** 
     * @Route("/reset-confirm/{passwordToken}", name="dziuba_user_password_reset_token")
     */
    public function passwordResetToken(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        
        $form = $this->createForm(UserType::class, $user);
        $form->remove('username');
        $form->remove('email');

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            //return $this->redirectToRoute('login');
        }

        return $this->render(
            '@DziubaUser/security/password.reset.confirm.html.twig', [
                'form' => $form->createView()
            ]);
    }
}