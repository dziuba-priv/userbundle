<?php

namespace Dziuba\UserBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;


class DziubaUserExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            //new FileLocator('@DziubaUserBundle/Resources/config')
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');;
    }
}

?>