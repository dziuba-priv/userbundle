Installation
============

Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```console
$ composer require dziuba/user-bundle
```

Applications that don't use Symfony Flex
----------------------------------------

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require dziuba/user-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Dziuba\UserBundle\DziubaUserBundle(),
        );

        // ...
    }

    // ...
}
```

### Step 3: Configure
Paste in routing.yaml
```yaml
dziuba_user_bundle:
    resource: '@DziubaUserBundle/Resources/config/routing.yaml'
```

Paste in packages/easy_admin.yaml
```yaml
easy_admin:
    #site_name: <i>d</i>Admin Panel
    site_name: '<img title="Panel administracyjny" alt="Panel administracyjny" src="../img/admin-logo.png" />'
    design:
        brand_color: '#2b5f77'
        color_scheme: 'light'
        assets:
          css: ['css/easyadmin.css']
          favicon: 'admin-favicon.png'
        menu: 
          - {entity: Page, icon: "file"}
          - {entity: User, icon: "users"}
    formats:
        date:     'd.m.Y'
        time:     'H:i'
        datetime: 'd.m.Y H:i:s'
    entities:
      Page:
        class: App\Entity\Page  
        label: page.pages
        list:
          title: page.pages
          fields:
            - { property: 'title', label: 'page.title', css_class: 'col-sm-7'}
            - { property: 'slug', label: 'page.slug', css_class: 'col-sm-1'}
            - { property: 'draft', label: 'page.draft', type: 'toggle', css_class: 'col-sm-1'}
            - { property: 'contentchanged', label: 'page.contentchanged', css_class: 'col-sm-1'}
            - { property: 'created', label: 'page.created', css_class: 'col-sm-1'}
          sort: ['updated', 'ASC']
          actions:
            #- { name: 'show', icon: 'search' }
            - { name: 'edit', icon: 'pencil' }
            - { name: 'delete', icon: 'trash-o' }
            - { name: 'new', icon: 'plus' }
        search:
          sort: ['updated', 'ASC']
        edit:
          title: page.edit
          fields:
            - { property: 'title', label: 'page.title'}
            - { property: 'draft', label: 'page.draft'}
            - { property: 'body', label: 'page.body', type: 'textarea'}
        new:
          title: page.add_new
          fields:
            - { property: 'title', label: 'page.title'}
            - { property: 'draft', label: 'page.draft'}
            - { property: 'body', label: 'page.body', type: 'textarea'}
      User:     
        class: Dziuba\UserBundle\Entity\User
        label: user.users
        form:
          fields:
            - { property: username, label: user.username}
            - {  property: email, label: user.email}
            - { property: isactive, label: user.enabled}
            #- { property: lastLogin, label: user.lastLogin }
            # if administrators are allowed to edit users' passwords and roles, add this:
            - { property: 'plainPassword', type: 'text', type_options: { required: false }, label: user.password }
            #- { property: 'roles', type: 'choice', type_options: { multiple: true, choices: { 'ROLE_USER': 'ROLE_USER', 'ROLE_ADMIN': 'ROLE_ADMIN' } }, label: user.roles }
        list:
          title: user.users
          actions:
            #- { name: 'show', icon: 'search' }
            - { name: 'edit', icon: 'pencil' }
            - { name: 'delete', icon: 'trash-o' }
            - { name: 'new', icon: 'user-plus' }
          fields: 
            - { property: id, label: 'ID', css_class: 'col-sm-1'}
            - { property: username, label: user.username, css_class: 'col-sm-3'}
            - { property: email, label: user.email, type: 'email', css_class: 'col-sm-3'}
            - { property: isactive, label: user.enabled, css_class: 'col-sm-1', type: 'toggle'}
            - { property: created, label: user.created, type: 'date', css_class: 'col-sm-1'}
            #- {property: lastLogin, label: user.lastLogin}
        show:
          title: user.show.title
          fields:
            - { property: username, label: user.username}
            - { property: email, label: user.email}
            - { property: isactive, label: user.enabled, type: 'boolean'}
            #- { property: lastLogin, label: user.lastLogin }
            - { property: 'roles', label: user.roles }
            - { property: 'created', label: user.created }
            - { property: 'updated', label: user.updated }
            - { property: 'confirmationToken', label: user.confirmationToken }
        edit:
          title: user.edit.title
          fields:
            - { property: 'isactive', type: 'checkbox'}
            # ...
            - { property: 'plainPassword', type_options: { required: false} }
            # ...
          #form_options: { validation_groups: ['Profile'] }
        new:
          title: user.add.title
          fields:
            - { property: 'isactive', type: 'checkbox'}
            # ...
            - { property: 'plainPassword', type_options: { required: true} }
            # ...
          #form_options: { validation_groups: ['Registration'] }
```